__author__ = 'raniba'

import json
import pandas as pd
import matplotlib.pyplot as plt

import re

tweets_data_path = 'data/mozsprint_raw_tweets.txt'

tweets_data = []
tweets_file = open(tweets_data_path, "r")
for line in tweets_file:
    try:
        tweet = json.loads(line)
        mozsprint_data.append(tweet)
    except:
        continue


#create the dataframe that will contain the tweets records

tweets = pd.DataFrame()
tweets['created_at'] = map(lambda tweet: tweet['created_at'], tweets_data)
tweets['user']= map(lambda tweet: tweet['user']['screen_name'], tweets_data)
tweets['text'] = map(lambda tweet: tweet['text'], tweets_data)
tweets['lang'] = map(lambda tweet: tweet['lang'], tweets_data)
tweets['country'] = map(lambda tweet: tweet['place']['country'] if tweet['place'] != None else None, tweets_data)
tweets['retweet_count'] = map(lambda tweet: tweet['retweet_count'], tweets_data)
tweets['favorite_count'] = map(lambda tweet: tweet['favorite_count'], tweets_data)


"""
Example of what we can do

tweets_by_lang = tweets['lang'].value_counts()

fig, ax = plt.subplots()
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=10)
ax.set_xlabel('Languages', fontsize=15)
ax.set_ylabel('Number of tweets' , fontsize=15)
ax.set_title('Top 5 languages', fontsize=15, fontweight='bold')
tweets_by_lang[:5].plot(ax=ax, kind='bar', color='red')
"""


#General plotting function for the different information extracted
def plot_tweets_per_category(category, title, x_title, y_title, top_n=5):
    """
    :param category: Category plotted, can be tweets users, tweets language, tweets country etc ..
    :param title: Title of the plot
    :param x_title: List of the items in x
    :param y_title: Title of the variable plotted
    :return: a plot that we can save as pdf or png instead of displaying to the screen
    """
    tweets_by_lang = category.value_counts()
    fig, ax = plt.subplots()
    ax.tick_params(axis='x', labelsize=15)
    ax.tick_params(axis='y', labelsize=10)
    ax.set_xlabel(x_title, fontsize=15)
    ax.set_ylabel(y_title , fontsize=15)
    ax.set_title(title, fontsize=15, fontweight='bold')
    tweets_by_lang[:top_n].plot(ax=ax, kind='bar', color='red')

#Mine specific informations that can exist in tweets, this will indicate if a topic is recurrent

def word_in_text(word, text):
    word = word.lower()
    text = text.lower()
    match = re.search(word, text)
    if match:
        return True
    return False


#context global variable
tweets['open'] = tweets['text'].apply(lambda tweet: word_in_text('openscience', tweet))
tweets['data'] = tweets['text'].apply(lambda tweet: word_in_text('data', tweet))
tweets['hackathon'] = tweets['text'].apply(lambda tweet: word_in_text('hackathon', tweet))
tweets['reproducible'] = tweets['text'].apply(lambda tweet: word_in_text('reproducible', tweet))
tweets['research'] = tweets['text'].apply(lambda tweet: word_in_text('research', tweet))

#which programming language is more popular ?
tweets['python'] = tweets['text'].apply(lambda tweet: word_in_text('python', tweet))
tweets['javascript'] = tweets['text'].apply(lambda tweet: word_in_text('javascript', tweet))
tweets['rstats'] = tweets['text'].apply(lambda tweet: word_in_text('rstats', tweet))


"""
try:
    print tweets['python'].value_counts()[True]
except:
    print 0

print tweets['javascript'].value_counts()[True]
print tweets['ruby'].value_counts()[True]
"""

def count_presence(variable):
    try:
        return tweets[variable].value_counts()[True]
    except:
        return 0

context = ['open', 'data', 'hackathon', 'reproducible', 'research', 'collaborate']
tweets_by_context = [count_presence(item) for item in context]





df = pd.DataFrame(tweets['created_at'].value_counts(), columns=['number_tweets'])
df['date'] = df.index
df.head()
days = [item.split(" ")[0] for item in df['date'].values]
df['days'] = days
grouped_tweets = df[['days', 'number_tweets']].groupby('days')
tweet_growth = grouped_tweets.sum()
tweet_growth['days']= tweet_growth.index

import numpy as np
fig = plt.figure()
ax = plt.subplot(111)
x_pos = np.arange(len(tweet_growth['days'].values))
ax.bar(x_pos, tweet_growth['number_tweets'].values, align='center')
ax.set_xticks(x_pos)
ax.set_title('#mozfest hashtag growth')
ax.set_ylabel("number tweets")
ax.set_xticklabels(tweet_growth['days'].values)
fig.savefig('plots/mozfest_growth.png')