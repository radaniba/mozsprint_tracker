__author__ = 'Rad'

# Import the necessary methods from tweepy library
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import API
from tweepy import Cursor
import json
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib import rcParams
from mpltools import style
from matplotlib import dates
from datetime import datetime
import seaborn as sns
import time
import os

from scipy.misc import imread
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import random

sns.set_palette("deep", desat=.6)
sns.set_context(rc={"figure.figsize": (8, 4)})

# for R lovers :)
style.use('ggplot')
rcParams['axes.labelsize'] = 9
rcParams['xtick.labelsize'] = 9
rcParams['ytick.labelsize'] = 9
rcParams['legend.fontsize'] = 7
# rcParams['font.family'] = 'serif'
rcParams['font.serif'] = ['Computer Modern Roman']
rcParams['text.usetex'] = False
rcParams['figure.figsize'] = 20, 10




# Variables that contains the user credentials to access Twitter API
access_token = "57309864-yMqpAwr8dkkVa0F2ly3dr49gawHbx5MH5BbmONKEo"
access_token_secret = "Pk2b5muUbrsAsJSVKUbcmero1MDabO1swjoLQiZB78GCH"
consumer_key = "EAUofwA8dzhcIBp3fPfuZWHgr"
consumer_secret = "pbcUtg76fR1mAXga06HaVitZBiPznPM0eUktEbaot8yni53M4v"

# Global variables

MAX_TWEETS = 8000

'''
We will not use the listner and the stream, we will rather get the
data through the query api because it is faster

The streaming is probably good for realtime monitoring

# This is a basic listener that just prints received tweets to stdout.
class StdOutListener(StreamListener):
    def on_data(self, data):
        print data
        return True

    def on_error(self, status):
        print status

    def on_status(self, status):
        if '' in status.text.lower() and 'retweeted_status' not in status:
            print status.text
            print status.coordinates

'''

if __name__ == '__main__':

    # This handles Twitter authentication and the connection to Twitter Streaming API
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = API(auth, wait_on_rate_limit=True)

    data = Cursor(api.search, q='mozsprint').items(MAX_TWEETS)

    mozsprint_data = []
    current_working_dir = os.path.dirname(os.path.realpath(__file__))
    log_tweets = current_working_dir + "/data/" + str(time.time()) + '_moztweets.txt'

    with open(log_tweets, 'w') as outfile:
        for tweet in data:
            mozsprint_data.append(json.loads(json.dumps(tweet._json)))
            outfile.write(json.dumps(tweet._json))
            outfile.write("\n")

    # Create the dataframe we will use
    tweets = pd.DataFrame()
    # tweets['created_at'] = map(lambda tweet: tweet['created_at'], mozsprint_data)
    tweets['created_at'] = map(lambda tweet: time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(tweet['created_at'],
                                                                                              '%a %b %d %H:%M:%S +0000 %Y')),
                               mozsprint_data)
    tweets['user'] = map(lambda tweet: tweet['user']['screen_name'], mozsprint_data)
    tweets['user_followers_count'] = map(lambda tweet: tweet['user']['followers_count'], mozsprint_data)
    tweets['text'] = map(lambda tweet: tweet['text'].encode('utf-8'), mozsprint_data)
    tweets['lang'] = map(lambda tweet: tweet['lang'], mozsprint_data)
    tweets['Location'] = map(lambda tweet: tweet['place']['country'] if tweet['place'] != None else None,
                             mozsprint_data)
    tweets['retweet_count'] = map(lambda tweet: tweet['retweet_count'], mozsprint_data)
    tweets['favorite_count'] = map(lambda tweet: tweet['favorite_count'], mozsprint_data)

    print tweets['user'].value_counts()
    print tweets['created_at'].value_counts()

    text = " ".join(tweets['text'].values.astype(str))
    d = os.path.dirname(__file__)

    def grey_color_func(word, font_size, position, orientation, random_state=None, **kwargs):
        return "hsl(0, 0%%, %d%%)" % random.randint(60, 100)

    no_urls_no_tags = " ".join([word for word in text.split()
                            if 'http' not in word
                                and not word.startswith('@')
                                and word != 'RT'
                            ])

    moz_mask = imread("./twitter_mask.png", flatten=True)
    wc = WordCloud(background_color="white", font_path="/Library/Fonts/Verdana.ttf", stopwords=STOPWORDS, width=1800,
                      height=140, mask=moz_mask)
    wc.generate(no_urls_no_tags)
    plt.imshow(wc)
    plt.axis("off")
    plt.savefig('plots/mozsprint.png', dpi=300)
    #plt.show()

    # default_colors = wc.to_array()
    # plt.title("Custom colors")
    # plt.imshow(wc.recolor(color_func=grey_color_func, random_state=3))
    # wc.to_file("mozsprint.png")
    # plt.axis("off")
    # plt.figure()
    # plt.title("Default colors")
    # plt.imshow(default_colors)
    # plt.axis("off")
    # plt.show()


    # General plotting function for the different information extracted
    def plot_tweets_per_category(category, title, x_title, y_title, top_n=5, output_filename="plot.png"):
        """
        :param category: Category plotted, can be tweets users, tweets language, tweets country etc ..
        :param title: Title of the plot
        :param x_title: List of the items in x
        :param y_title: Title of the variable plotted
        :return: a plot that we can save as pdf or png instead of displaying to the screen
        """
        tweets_by_cat = category.value_counts()
        fig, ax = plt.subplots()
        ax.tick_params(axis='x')
        ax.tick_params(axis='y')
        ax.set_xlabel(x_title)
        ax.set_ylabel(y_title)
        ax.set_title(title)
        tweets_by_cat[:top_n].plot(ax=ax, kind='bar')
        fig.savefig(output_filename)

    #plot_tweets_per_category(tweets['lang'], "#mozsprint by Language", "Language", "Number of Tweets", 2000,
    #                         "plots/mozsprint_per_language.png")
    #plot_tweets_per_category(tweets['Location'], "#mozsprint by Location", "Location", "Number of Tweets", 2000,
    #                         "plots/mozsprint_per_location.png")
    #plot_tweets_per_category(tweets['user'], "#mozsprint active users", "Users", "Number of Tweets", 20,
    #                         "plots/mozsprint_users.png")


    # plot the distribution of retweets
    def plot_distribution(category, title, x_title, y_title, output_filename="plot.png"):
        """
        :param category: Category plotted, can be tweets users, tweets language, tweets country etc ..
        :param title: Title of the plot
        :param x_title: List of the items in x
        :param y_title: Title of the variable plotted
        :return: a plot that we can save as pdf or png instead of displaying to the screen
        """
        fig, ax = plt.subplots()
        ax.tick_params(axis='x')
        ax.tick_params(axis='y')
        ax.set_xlabel(x_title)
        ax.set_ylabel(y_title)
        ax.set_title(title)
        sns.distplot(category.values, rug=True, hist=False);
        fig.savefig(output_filename)

    #plot_distribution(tweets['retweet_count'], "#mozsprint retweets distribution", "", "",
    #                  "plots/retweets_distribution.png")
    #plot_distribution(tweets['favorite_count'], "#mozsprint favorites distribution", "", "",
    #                  "plots/favorites_distribution.png")

    def plot_regplot(category1, category2, df, title, x_title, y_title, output_filename="plot.png"):
        """
        :param category1: First variable name
        :param category2: Second variable name
        :param df: The dataframe containing the data
        :param title: Title of the plot
        :param x_title: X axis label
        :param y_title: Y axis label
        :param output_filename: File to save the plot to
        :return:  a scatter plot between x and y with a regression line.
        """
        fig, ax = plt.subplots()
        ax.tick_params(axis='x')
        ax.tick_params(axis='y')
        ax.set_xlabel(x_title)
        ax.set_ylabel(y_title)
        ax.set_title(title)
        sns.regplot(category1, category2, df)
        fig.savefig(output_filename)

    #plot_regplot("retweet_count", "favorite_count", tweets, "#mozsprint retweets vs favorites", "", "",
    #             "plots/reg_plot.png")
    #plot_regplot("user_followers_count", "retweet_count", tweets, "#mozsprint retweets / followers base relationship", "", "",
    #             "plots/reg_plot_retweets_followers_base.png")

    def plot_joint_plot(category1, category2, df, title, x_title, y_title, output_filename="plot.png"):
        fig, ax = plt.subplots()
        ax.tick_params(axis='x')
        ax.tick_params(axis='y')
        ax.set_xlabel(x_title)
        ax.set_ylabel(y_title)
        ax.set_title(title)
        sns.jointplot(category1, category2, df, marginal_kws={"bins": 10}, kind="reg");
        fig.savefig(output_filename)

    # plot_joint_plot("retweet_count", "favorite_count", tweets,  "#mozsprint retweets vs favorites joinplot", "","", "join_plot.png")

    days = dates.DayLocator()
    hours = dates.HourLocator()
    dfmt = dates.DateFormatter('%b %d')
    datemin = datetime(2015, 2, 1, 0, 0)
    datemax = datetime(2015, 7, 10, 0, 0)

    # print tweets['created_at'].values.split(" ")[0]
    # print tweets['created_at'].value_counts()

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.xaxis.set_major_locator(days)
    ax.xaxis.set_major_formatter(dfmt)
    ax.xaxis.set_minor_locator(hours)
    ax.set_xlim(datemin, datemax)
    ax.set_ylabel('Temperature (F)')
    # ax.plot(tweets['created_at'].values, tweets['retweet_count'].values, linewidth=2)
    fig.set_size_inches(8, 4)
    # plt.savefig('time.png')
